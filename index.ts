import express from "express";
import db from "./models";
import { users } from "./seeders/users";
import { projects } from "./seeders/projects";
import { projectassignments } from "./seeders/projectassignments";

const app = express();
const port = process.env.PORT || 3000;

// const createUsers = () => {
//     users.map((user) => {
//         db.User.create(user);
//     });
// };
// createUsers();

// const createProjects = () => {
//     projects.map((project) => {
//         db.Project.create(project);
//     });
// };
// createProjects();

// const createProjectAssigments = () => {
//     projectassignments.map((ps) => {
//         db.ProjectAssignment.create(ps);
//     });
// };
// createProjectAssigments();

app.get("/", (req, res) => {
    db.User.findAll({
        include: {
            model: db.Project,
        },
    })
        .then((result: object) => res.send(result))
        .catch((err: object) => console.log(err));
});

/**
 * Model synchronization
 * https://sequelize.org/master/manual/model-basics.html#model-synchronization
 */
db.sequelize.sync().then(() => {
    app.listen(port, () => {
        console.log(`App listen on port ${port}`);
    });
});
