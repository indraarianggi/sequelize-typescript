# Learn Sequelize using TypeScript

## How to run this app?

1. Clone this repository

    ```
    git clone https://gitlab.com/indraarianggi/sequelize-typescript.git
    ```

2. Install dependencies

    ```
    yarn install
    ```

3. Create `.env` file and copy from `.env.example` file. Change the value with your config.

4. Run this command to create database

    ```
    npx sequelize-cli db:create
    ```

5. Run the app with this command

    ```
    yarn dev
    ```

## Resources

1. [Using Sequelize With TypeScript: Basic Project Setup](https://www.youtube.com/watch?v=VyEKwp6Q4fY)
2. [Sequelize v6 Documentation](https://sequelize.org/master/)
