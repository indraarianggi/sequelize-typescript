import { ProjectAssignmentAttributes } from "../models/projectassignment";

export const projectassignments: ProjectAssignmentAttributes[] = [
    {
        ProjectId: 1,
        UserId: "4fe19108-1e6a-4289-81fb-b9c860a1e4c0",
    },
    {
        ProjectId: 3,
        UserId: "4fe19108-1e6a-4289-81fb-b9c860a1e4c0",
    },
    {
        ProjectId: 1,
        UserId: "8965f238-eed4-4a10-b791-004c22c3cb2c",
    },
];
