import { ProjectAttributes } from "../models/project";

export const projects: Omit<ProjectAttributes, "id">[] = [
    {
        title: "Site Upgrade - Login Page",
        status: "active",
    },
    {
        title: "Site Upgrade - User Dashboard",
        status: "active",
    },
    {
        title: "Database Maintenance",
        status: "complete",
    },
];
