import { v4 as uuidv4 } from "uuid";
import { UserAttributes } from "../models/user";

export const users: UserAttributes[] = [
    {
        id: uuidv4(),
        name: "John Doe",
        email: "johndoe@gmail.com",
        password: "12345",
    },
    {
        id: uuidv4(),
        name: "Octavio Flores",
        email: "fbennis@gmail.com",
        password: "abc123",
    },
];
